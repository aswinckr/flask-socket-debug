from flask import (
    Blueprint,
    session,
    redirect,
    url_for,
    request,
    render_template)

from highcampers.blueprints.chat.forms import LoginForm

chat = Blueprint('chat', __name__, template_folder='templates')


@chat.route('/chat', methods=['GET', 'POST'])
def index():
    """"Login form to enter a room."""
    form = LoginForm()
    if form.validate_on_submit():
        session['name'] = form.name.data
        session['room'] = form.room.data
        return redirect(url_for('chat.chatroom'))
    elif request.method == 'GET':
        form.name.data = session.get('name', '')
        form.room.data = session.get('room', '')
    return render_template('index.html', form=form)


@chat.route('/chat/chatroom')
def chatroom():
    """Chat room. The user's name and room must be stored in
    the session."""
    name = session.get('name', '')
    room = session.get('room', '')
    if name == '' or room == '':
        return redirect(url_for('chat.index'))
    return render_template('chat.html', name=name, room=room)

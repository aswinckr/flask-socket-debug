from sqlalchemy import func

from highcampers.blueprints.user.models import User, db


class Dashboard(object):
    @classmethod
    def group_and_count_users(cls):
        '''
        
        Perform group and count for the users
        
        :return: dict 
        '''
        return Dashboard._group_and_count(User, User.role)

    @classmethod
    def _group_and_count(cls, model, field):
        '''
        Group results for a specific model and field
        
        :param model: Name of the Model
        :type model: SQLAlchemy Model
        :param field: Name of the field to group on
        :type field: SQLAlchemy Field
        :return: 
        '''

        count = func.count(field)
        query = db.session.query(count, field).group_by(field).all()

        results = {
            'query': query,
            'total': model.query.count()
        }

        return results
from flask_wtf import Form
from wtforms import HiddenField, StringField, PasswordField
from wtforms_components import EmailField, Email, Unique
from wtforms.validators import DataRequired, Length, Regexp, Optional
from highcampers.blueprints.user.models import User, db
from lib.util_wtforms import ModelForm
from validators import ensure_existing_password_matches, ensure_identity_exists


class LoginForm(Form):
    next = HiddenField()
    identity = StringField("Username or email",
                           [DataRequired(), Length(3, 254)])
    password = PasswordField("Password",
                             [DataRequired(), Length(8, 128)])


class SignupForm(ModelForm):
    username_message = 'Letters, numbers and underscores only please.'

    username = StringField(validators=[
        Unique(
            User.username,
            get_session=lambda: db.session
        ),
        DataRequired(),
        Length(1, 16),
        Regexp('^\w+$', message=username_message)
    ])
    email = EmailField(validators=[
        DataRequired(),
        Email(),
        Unique(
            User.email,
            get_session=lambda: db.session
        )
    ])
    password = PasswordField('Password',
                             [DataRequired(), Length(8, 128)])


class UpdateCredentialsForm(ModelForm):
    username_message = 'Letters, numbers and underscores only please.'

    fullname = StringField("Fullname")
    username = StringField(validators=[
        Unique(User.username,
               get_session=lambda: db.session
               ),
        Length(1, 16),
        Regexp('^\w+$', message=username_message)

    ])
    email = EmailField(validators=[
        Email(),
        Unique(
            User.email,
            get_session=lambda: db.session
        )
    ])


class UpdatePasswordForm(ModelForm):
    current_password = PasswordField('Current Password',
                                     [DataRequired(),
                                      Length(8, 128),
                                      ensure_existing_password_matches])
    password = PasswordField('Password', [Optional(),
                                              Length(8, 128)])


class BeginPasswordReset(Form):
    identity = StringField("Username or email",
                           [DataRequired(),
                            Length(3, 254),
                            ensure_identity_exists])


class PasswordResetForm(Form):
    reset_token = HiddenField()
    password = PasswordField('Password',
                             [DataRequired(), Length(8, 128)])


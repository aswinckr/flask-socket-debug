from lib.flask_mailplus import send_template_message
from highcampers.app import create_celery_app
from highcampers.blueprints.user.models import User

celery = create_celery_app()


@celery.task()
def email_verification(user_id, confirmation_token):
    '''
    
    :param user_id: 
    :param confirmation_token:  
    :return: None
    '''
    user = User.query.get(user_id)

    if user is None:
        return

    ctx = {'user': user, 'confirmation_token': confirmation_token}

    send_template_message(subject='Verify your email address',
                          recipients=[user.email],
                          template='user/mail/email_verification', ctx=ctx
                          )

    return None


@celery.task()
def deliver_welcome_email(email, username):
    """
    Send a welcome email to signing up users.

    :param username: 
    :param email: E-mail address of the visitor
    :return: None
    """
    ctx = {'email': email, 'username': username}

    send_template_message(subject='Welcome to Highcampers',
                          sender=str([celery.conf.get('MAIL_USERNAME')]),
                          recipients=email.split(),
                          reply_to=email,
                          template='user/mail/index', ctx=ctx
                          )

    return None


@celery.task()
def deliver_password_reset_email(user_id, reset_token):
    """
    Send password reset email to forgetful users.

    :param reset_token: 
    :param user_id: 
    :return: None
    """
    user = User.query.get(user_id)

    if user is None:
        return

    ctx = {'user': user, 'reset_token': reset_token}

    send_template_message(subject='Reset Password from Highcampers',
                          recipients=[user.email],
                          template='user/mail/begin_password_reset', ctx=ctx
                          )

    return None

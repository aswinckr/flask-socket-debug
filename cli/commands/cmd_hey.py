import subprocess

import click


@click.command()
@click.argument('name', default='Aswin')
def cli(name):
    """
    Say Hello

    :return: This says hello
    """
    cmd = 'Hey {0}!'.format(name)
    # return subprocess.call(cmd, shell=True)
    print(str(cmd))